package testsuite

import (
	"fmt"
	"github.com/nemunaire/eyespot"
	"github.com/spacemonkeygo/openssl"
)

type Certificates struct {}

func (Certificates) GetTestDescription() string {
	return "Test the certificate of the remote host."
}

func (test Certificates) Run(protocol string, host string) (map[string]eyespot.Result, error) {
	var results = map[string]eyespot.Result{}

	ctx, err := openssl.NewCtx()
	if err != nil {
		return results, err
	}

	conn, err := openssl.Dial(protocol, host, ctx, openssl.InsecureSkipHostVerification)
	if err != nil {
		return results, err
	}
	defer conn.Close();

//	if err := conn.Handshake(); err != nil {
//		return err
//	}

	cert, err := conn.PeerCertificate()

	if err != nil {
		return results, err
	}

	nm, err := cert.GetIssuerName()

	if err != nil {
		return results, err
	}

	if str, ok := nm.GetEntry(13); ok != false {
		fmt.Printf(str)
	}

	return results, nil
}
