package main

import (
	"flag"
	"fmt"
	"log"
	"github.com/nemunaire/eyespot"
	"github.com/nemunaire/eyespot/testsuite"
)

var tests = []eyespot.Test{
	testsuite.Protocols{},
	testsuite.Ciphers{},
}

func main() {
	var protocol = flag.String("protocol", "tcp", "Protocol to test")
	var hostname = flag.String("hostname", "localhost", "Hostname to test")
	var port = flag.Int("port", 443, "Port to test")
	flag.Parse()

	host := fmt.Sprintf("%s:%d", *hostname, *port)

	for _, t := range tests {
		log.Println(t.GetTestDescription())

		if res, err := t.Run(*protocol, host); err != nil {
			log.Println(err)
		} else {
			log.Println(res)
		}
	}
}
