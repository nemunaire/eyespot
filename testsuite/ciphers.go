package testsuite

import (
	"github.com/nemunaire/eyespot"
	"github.com/spacemonkeygo/openssl"
)

type Ciphers struct {}

func (Ciphers) GetTestDescription() string {
	return "Test the ciphers suite accepted by the remote host."
}

func (test Ciphers) Run(protocol string, host string) (map[string]eyespot.Result, error) {
	var results = map[string]eyespot.Result{}

	for _, c := range []string{
		"RC4-SHA",
		"DES-CBC3-SHA",
		"AES128-SHA",
		"AES256-SHA",
		"ECDHE-ECDSA-RC4-SHA",
		"ECDHE-ECDSA-AES128-SHA",
		"ECDHE-ECDSA-AES256-SHA",
		"ECDHE-RSA-RC4-SHA",
		"ECDHE-RSA-DES-CBC3-SHA",
		"ECDHE-RSA-AES128-SHA",
		"ECDHE-RSA-AES256-SHA",
		"ECDHE-RSA-AES128-GCM-SHA256",
		"ECDHE-ECDSA-AES128-GCM-SHA256",
	} {
		if r, err := cipher_test(protocol, host, c); err != nil {
			return results, err
		} else {
			results[c] = eyespot.Result{r}
		}
	}

	return results, nil
}

func cipher_test(protocol string, host string, cipher string) (bool, error) {
	ctx, err := openssl.NewCtx()

	if err != nil {
		return false, err
	}

	if err := ctx.SetCipherList(cipher); err != nil {
		return false, err
	}

	conn, err := openssl.Dial(protocol, host, ctx, openssl.InsecureSkipHostVerification)
	if err != nil {
		return false, nil
	}
	defer conn.Close();

	return true, nil
}
