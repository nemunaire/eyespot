package eyespot

type Result struct {
	Passed bool
}

type Test interface {
	GetTestDescription() string

	Run(protocol string, host string) (map[string]Result, error)
}
