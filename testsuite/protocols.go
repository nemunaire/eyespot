package testsuite

import (
	"github.com/nemunaire/eyespot"
	"github.com/spacemonkeygo/openssl"
)

type Protocols struct {
}

func (Protocols) GetTestDescription() string {
	return "Test for protocols accepted by the remote host."
}

func (test Protocols) Run(protocol string, host string) (map[string]eyespot.Result, error) {
	var results = map[string]eyespot.Result{}

	for _, v := range []openssl.SSLVersion{
		//0x01, // openssl.SSLv2
		openssl.SSLv3,
		openssl.TLSv1,
		openssl.TLSv1_1,
		openssl.TLSv1_2,
	} {
		if r, err := protocol_test(protocol, host, v); err != nil {
			return results, err
		} else {
			var cstr string

			switch v {
			case 0x01:
				cstr = "SSLv2"
			case openssl.SSLv3:
				cstr = "SSLv3"
			case openssl.TLSv1:
				cstr = "TLSv1"
			case openssl.TLSv1_1:
				cstr = "TLSv1.1"
			case openssl.TLSv1_2:
				cstr = "TLSv1.2"
			default:
				cstr = ""
			}

			results[cstr] = eyespot.Result{r}
		}
	}

	return results, nil
}

func protocol_test(protocol string, host string, version openssl.SSLVersion) (bool, error) {
	ctx, err := openssl.NewCtxWithVersion(version)

	if err != nil {
		return false, err
	}

	conn, err := openssl.Dial(protocol, host, ctx, openssl.InsecureSkipHostVerification)
	if err != nil {
		return false, nil
	}
	defer conn.Close();

	return true, nil
}
